Para evitarte problemas, te recomiendo reemplazar npm por yarn, que es otro gestor de paquetes de Node más moderno, y que tiene muy buena crítica. Puedes leer más sobre Yarn, aquí: https://yarnpkg.com/es-ES/


Solo tienes que seguir los siguientes pasos:

·1. Instalar yarn:
Como tienes npm instalado, puedes usarlo para instalar yarn :)
Desde termina, solo tienes que hacer:

    npm install -g yarn


·2. Instalar la CLI de Angular en Yarn

Tienes que instalar la CLI de Angular a nivel global, pero esta vez en Yarn.
Así:

    yarn global add @angular/cli 


·3. Utilizar yarn en vez de npm para instalar las dependencias de cada proyecto

Al descargar el código fuente del proyecto, verás que te pido que instales las dependencias del proyecto con npm install.
Pues bien, en lugar de usar ese comando, utiliza yarn para instalar las dependencias, así: yarn install 