import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReservasListadoComponent } from './reservas-listado/reservas-listado.component';
import { ReservasDestalleComponent } from './reservas-destalle/reservas-destalle.component';

const routes: Routes = [
  { path: 'reservas', component: ReservasListadoComponent },
  { path: 'reservas/:id', component: ReservasDestalleComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservasRoutingModule { }
