# README #

* Copiar el proyecto con 
git clone https://bitbucket.org/SirMaster/curso-angular6.git

* Ingresar a la carpeta del proyecto
cd angular-hola-mundo

* Instalar dependencias con cmdline
npm install 

* Con dentro de la carpeta angular-hola-mundo ejecutar
ng serve

* Con otra instancia de cmd ingresar a
cd express-server

* levantar el servido API
node app.js